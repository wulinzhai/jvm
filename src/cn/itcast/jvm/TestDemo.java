package cn.itcast.jvm;

import org.junit.Test;

import java.util.ArrayList;

public class TestDemo {

    @Test
    public void f(){
        System.out.println(System.currentTimeMillis());
    }

    @Test
    public void f1(){
        ArrayList<Object> list = new ArrayList<>();
        list.clear();
        System.out.println(list);   //[]
    }


    @Test
    public void f2(){
//        int i = Integer.parseInt("12");
//        int integer = Integer.valueOf("12");


        int i = 128;
        Integer i2 = 128;
        Integer i3 = new Integer(128);
        //Integer会自动拆箱为int，所以为true
        System.out.println(i == i2);
        System.out.println(i == i3);

        System.out.println(i2 == i3);

        Integer i4 = new Integer(10);
        Integer i5 = new Integer(10);
        System.out.println(i4.equals(i5));
        System.out.println(i5 == i4);




    }


    @Test
    public void f3(){

    }

}
